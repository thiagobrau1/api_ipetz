import Sequelize, { Model } from 'sequelize';

class Produtos extends Model {
  static init(sequelize) {
    super.init(
      {
        id_lojista: Sequelize.INTEGER,
        nome_produto: Sequelize.STRING,
        categoria: Sequelize.STRING,
        valor: Sequelize.STRING,
        foto_produto: Sequelize.STRING,
        descricao_produto: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );
    return this;
  }
}

export default Produtos;
