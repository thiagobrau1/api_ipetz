import Sequelize, { Model } from 'sequelize';

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        password_hash: Sequelize.STRING,
        provider: Sequelize.BOOLEAN,
        nomeloja: Sequelize.STRING,
        fotoloja: Sequelize.STRING,
        cpf_cnpj: Sequelize.STRING,
        telefone: Sequelize.STRING,
        endereco: Sequelize.STRING,
        estado: Sequelize.STRING,
        cidade: Sequelize.STRING,


      },
      {
        sequelize,
      }
    );
  }
}
export default User;
