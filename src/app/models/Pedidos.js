import Sequelize, { Model } from 'sequelize';

class Pedidos extends Model {
  static init(sequelize) {
    super.init(
      {
        id_cliente: Sequelize.INTEGER,
        id_lojista: Sequelize.INTEGER,
        id_produto: Sequelize.INTEGER,
        categoria: Sequelize.STRING,
        valor: Sequelize.STRING,
        nome_cliente: Sequelize.STRING,
        nome_produto: Sequelize.STRING,
        descricao_produto: Sequelize.STRING,
        data_compra: Sequelize.DATE,
        status: Sequelize.INTEGER,

      },
      {
        sequelize,
      }
    );
    return this;
  }
}

export default Pedidos;
