import Produto from '../models/Produtos';
import User from '../models/User';

class ProdutoController {
  async store(req, res) {

    const produtos = await Produto.create(req.body);

    return res.json(produtos);
  }

  async produtos(req, res) {
  const id = req.body

    const produtos = await Produto.findAll({where: { id_lojista : id.id_lojista}});

    return res.json(produtos);
  }

  async produtosbyID(req, res) {
    const id = req.body

    const produtos = await Produto.findOne({where: { id : id.id}});

    return res.json(produtos);
  }
}



export default new ProdutoController();
