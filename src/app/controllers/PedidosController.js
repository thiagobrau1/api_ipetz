import Pedidos from '../models/Pedidos';
import User from '../models/User';

class PedidosController {

  async store(req, res) {

    const pedidos = await Pedidos.create(req.body);

    return res.json(pedidos);
  }
  async pedidosCliente(req, res) {

    const cliente = req.body
    const pedidos = await Pedidos.findAll({where: { id_cliente : cliente.id_cliente}});

    return res.json(pedidos);
  }

  async pedidosLojista(req, res) {

    const lojista = req.body
    const pedidos = await Pedidos.findAll({where: { id_lojista : lojista.id_lojista}});

    return res.json(pedidos);
  }
  async updateStatus(req, res) {

    const id = req.body
    const status = req.body

    const pedidos = await Pedidos.update({status: status.status},{ where: {id: id.id}})



    return res.json(pedidos);
  }

}



export default new PedidosController();
