import User from '../models/User';

class UserController {
  async store(req, res) {
    const userExists = await User.findOne({where: { cpf_cnpj : req.body.cpf_cnpj}});

    if(userExists){
      return  res.json('false');
    }

    const user = await User.create(req.body);

    return res.json('true');
  }

  async login(req, res){

    const {cpf_cnpj, password_hash} = req.body;

    const user  = await User.findOne({ where :{cpf_cnpj}});

    const login  = await User.findOne({ where :{cpf_cnpj,password_hash}});

    if(!user){
      return res.json({status:400})
    }

    if(!login){
      return res.json({status:400})
    }

    return res.json(login);

  }

  async lojistas(req, res) {

    const lojistas = await User.findAll({where: { provider : 2}});

    return res.json(lojistas);
  }


}



export default new UserController();
