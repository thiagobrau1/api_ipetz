import Sequelize from 'sequelize';

import User from '../app/models/User';
import Produtos from '../app/models/Produtos';
import Pedidos from '../app/models/Pedidos';

import databaseConfig from '../config/database';

const models = [User,Produtos,Pedidos];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models.map((model) => model.init(this.connection));
  }
}

export default new Database();
