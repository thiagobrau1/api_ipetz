module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('pedidos', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      id_cliente: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      id_lojista: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      id_produto: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      categoria: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      valor: {
        type: Sequelize.STRING,
        defaultValue: false,
        allowNull: false,
      },
      nome_cliente: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      nome_produto: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      descricao_produto: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      data_compra: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('pedidos');
  },
};
