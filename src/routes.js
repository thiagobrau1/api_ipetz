import { Router } from 'express';

import UserController from './app/controllers/UserController';
import ProdutoController from './app/controllers/ProdutoController';
import PedidosController from './app/controllers/PedidosController';

const routes = new Router();

routes.post('/users', UserController.store);
routes.post('/login', UserController.login);
routes.get('/lojistas', UserController.lojistas);
routes.post('/produtos', ProdutoController.store);
routes.post('/produtoslojista', ProdutoController.produtos);
routes.post('/produtosbyid', ProdutoController.produtosbyID);
routes.post('/pedidos', PedidosController.store);
routes.post('/pedidosCliente', PedidosController.pedidosCliente);
routes.post('/pedidosLojista', PedidosController.pedidosLojista);
routes.put('/pedidosLojista', PedidosController.updateStatus);

export default routes;
